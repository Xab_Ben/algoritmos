#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "GA.h"
#include "EvaluateQAP.h"
#include "RandomPerm.h"
#include "globals.h"
#include "sort.h"

//Actualiza mejor solucion.
void check_best(int *solution, long double f0, strct_optimo *optimo){
  int i;
  if(optimo->opt_permu[0]==-1||f0<optimo->opt_fitness){
      for(i=0;i<PermuSize;i++){
          optimo->opt_permu[i] = solution[i];
      }
      optimo->opt_fitness=f0;
  }
}

//Crossover entre dos soluciones padres.
int * crossover(int * parent1, int * parent2){
    int i, j, point, aux, flag;
    int *child;
    child = malloc(PermuSize*sizeof(int));
    point = rand()%(PermuSize-1);
    for(i=0;i<=point;i++)
      child[i] = parent1[i];
    for(i=point+1;i<PermuSize;i++){
        aux = parent2[i];
        flag=1;
        while(flag){
            flag=0;
            for(j=0;j<=point;j++){
              if(aux==child[j]){
                  aux=parent2[j];
                  flag=1;
                  break;
              }
            }
        }
        child[i]=aux;
    }
    return child;
}

//Reproduccion.
int * reproduction(int *solutions[], int selected[], int sel_size){
    int i, j;
    i = rand()%sel_size;
    j = rand()%sel_size;
    while(i==j) j = rand()%sel_size;
    return crossover(solutions[selected[i]],solutions[selected[j]]);
}

//Torneo binario.
int binary_tournament(long double fitness [], int size){
    int i, j;
    i=rand()%size;
    j=rand()%size;
    while(i==j) j=rand()%size;
    if(fitness[i]<=fitness[j])
        return i;
    else
        return j;
}

//Mutacion swap.
void mutate(int * solution){
    int i, j, aux;
    i = rand()%PermuSize;
    j = rand()%PermuSize;
    while(i==j) j = rand()%PermuSize;
    aux = solution[i];
    solution[i] = solution[j];
    solution[j] = aux;
}


//Algoritmo genético básico.
void genetic_algorithm(strct_optimo *optimo, int pop_size, int sel_size, int off_size, double mutation_rate, int max_evals){
    int * population [pop_size];
    long double pop_fitness [pop_size];
    int * offspring[off_size];
    long double off_fitness [off_size];
    int selected [sel_size];
    int i, j, k, iteration, evals;

    optimo->opt_permu[0]=-1;

    //Se inicializa la población de forma aleatoria.
    evals=0;
    for(i=0;i<pop_size;i++){
        if(evals>=max_evals) break;
        population[i]=malloc(PermuSize*sizeof(int));
        RandomPerm(population[i]);
        pop_fitness[i] = EvaluateQAP(0,population[i]);
        evals+=1;
        //Se actualiza la mejor solución.
        check_best(population[i],pop_fitness[i],optimo);
    }

    iteration = 0;
    //Bucle principal.
    while(evals<max_evals){
        //printf("%d , %Lf\n",iteration,optimo->opt_fitness);
        iteration++;
        //Se seleccionan las soluciones candidatas para la reproducción.
        for(i=0;i<sel_size;i++){
            selected[i] = binary_tournament(pop_fitness,pop_size);
        }

        //Se crea la descendencia mediante la reproducción y la mutación.
        for(i=0;i<off_size;i++){
            if(evals>=max_evals){
                off_size=i;
                break;
            }
            offspring[i] = reproduction(population,selected,sel_size);
            if(mutation_rate > (double)rand()/(double)RAND_MAX) mutate(offspring[i]);
            off_fitness[i] = EvaluateQAP(0,offspring[i]);
            evals+=1;
        }

        //Nos quedamos con las mejores soluciones entre la población anterior y la nueva descendencia (Elitismo).
        bubbleSort(pop_fitness,population,pop_size);
        bubbleSort(off_fitness,offspring,off_size);
        i=pop_size-1;
        j=0;
        while(i>=0 && j<=off_size-1 && pop_fitness[i]>off_fitness[j]){
            for(k=0;k<PermuSize;k++)
                population[i][k] = offspring[j][k];
            pop_fitness[i] = off_fitness[j];
            //Se actualiza la mejor solución.
            check_best(population[i],pop_fitness[i],optimo);
            i--;
            j++;
        }
        for(i=0;i<off_size;i++){
            free(offspring[i]);
        }
     }
}
