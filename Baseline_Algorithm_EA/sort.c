#include "globals.h"
#include <stdlib.h>

//Swap de long doubles.
void swap1(long double *xp, long double *yp)
{
    long double temp = *xp;
    *xp = *yp;
    *yp = temp;
}

//Swap de enteros.
void swap2(int *xp, int *yp)
{
    int i;
    int *temp;

    temp = malloc(PermuSize*sizeof(int));
    for(i=0;i<PermuSize;i++){
      temp[i] = xp[i];
    }
    for(i=0;i<PermuSize;i++){
      xp[i] = yp[i];
    }
    for(i=0;i<PermuSize;i++){
      yp[i] = temp[i];
    }
    free(temp);
}

//Se ordenan dos arrays en base a uno de ellos.
void bubbleSort(long double arr1[], int *arr2[], int n)
{
    int i, j;
    for (i = 0; i < n-1; i++){
      for (j = 0; j < n-i-1; j++){
          if (arr1[j] > arr1[j+1]){
              swap1(&arr1[j], &arr1[j+1]);
              swap2(arr2[j], arr2[j+1]);
          }
      }
    }
}
