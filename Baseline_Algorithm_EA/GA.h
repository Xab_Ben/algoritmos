#ifndef __COM_H_
#define __COM_H_


typedef struct strct_optimo{
	int *opt_permu;
	long double opt_fitness;
}strct_optimo;

void genetic_algorithm(strct_optimo *optimo, int pop_size, int sel_size, int off_size, double mutation_rate, int max_evals);

#endif
