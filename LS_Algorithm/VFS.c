#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "VFS.h"
#include "list.h"
#include "EvaluateQAP.h"
#include "globals.h"


//Actualiza mejor solución.
void check_best(int* funcs, strct_optimo* optimo, strct_optimo* actual){
	int cost = EvaluateQAP(funcs[0], actual->opt_permu);
	if(cost < optimo->opt_fitness){
			optimo->opt_fitness=cost;
			memcpy(optimo->opt_permu, actual->opt_permu, sizeof(int)*PermuSize);
	}
}

//Excepción al criterio tabú.
int exception(int func, strct_optimo* optimo, long double cost_neigh){
		if(func==0 && cost_neigh < optimo->opt_fitness){
			return 1;
		}else{
			return 0;
		}
}

//Un paso de la búsqueda local en base a la función objetivo especificada.
int local_search (int* funcs, int func, int *evals, int max_evals, int iteration, strct_optimo* actual, strct_optimo* optimo, List* tabu, int tabu_size){
		long double cost_neigh;
		int i, j, best_i, best_j, aux, cont;

		//printf("%d , %Lf\n",iteration,optimo->opt_fitness);
		cont=0;
		//Exploramos el vecindario de la solución actual.
	  for (i=0; i<PermuSize-1; i++) {
				if(*evals>=max_evals) break;

	      for (j=i+1; j<PermuSize; j++) {
						if(*evals>=max_evals) break;

	          aux=actual->opt_permu[i];
	          actual->opt_permu[i]=actual->opt_permu[j];
	          actual->opt_permu[j]=aux;

	          cost_neigh=EvaluateQAP(funcs[func], actual->opt_permu);
						*evals+=1;
						//Nos quedamos con la mejor solución que mejore a la actual que no sea tabú.
	          if (cost_neigh < actual->opt_fitness && (!inlist(*tabu,i,j) || exception(func,optimo,cost_neigh))) {
	              best_i=i;
	              best_j=j;
	              actual->opt_fitness=cost_neigh;
								cont=1;
	          }

	          aux= actual->opt_permu[j];
	          actual->opt_permu[j]=actual->opt_permu[i];
	          actual->opt_permu[i]=aux;
	      }
	  }
		//Si hay una mejora, actualizamos la solución actual y añadimos el movimiento de vecindario a la lista tabú.
	  if (cont) {
	      aux=actual->opt_permu[best_i];
	      actual->opt_permu[best_i]=actual->opt_permu[best_j];
	      actual->opt_permu[best_j]=aux;
				insertback(tabu,best_i,best_j);
				if(length(*tabu) > tabu_size) popfront(tabu);
				//Actualizamos mejor solución.
				check_best(funcs, optimo, actual);
	  }
		return cont;
}

//Variable Function Search.
void VFS(int* funcs, strct_optimo* optimo, int tabu_size, int max_evals){
		int evals,iteration,cont,cost;
		strct_optimo actual;
		List* tabu;

		tabu = malloc(sizeof(List));
		initlist(tabu);

		actual.opt_permu=malloc(PermuSize*sizeof(int));
		memcpy(actual.opt_permu, optimo->opt_permu, sizeof(int)*PermuSize);

		iteration=1;
		evals=1;

		//Bucle principal.
		while(evals < max_evals){
				cont=1;
				actual.opt_fitness=EvaluateQAP(funcs[0], actual.opt_permu);
				//Búsqueda local en base a la función principal.
				while(cont && evals < max_evals){
					cont = local_search(funcs, 0, &evals, max_evals, iteration, &actual, optimo, tabu, tabu_size);
					iteration++;
				}
				//Escapamos del óptimo local con la primera función auxiliar.
				if(!cont && evals < max_evals && funcs[1]!=-1){
					actual.opt_fitness=EvaluateQAP(funcs[1], actual.opt_permu);
					cont = local_search(funcs, 1, &evals, max_evals, iteration, &actual, optimo, tabu, tabu_size);
					iteration++;
				}
				//Escapamos del óptimo local con la segunda función auxiliar.
				if(!cont && evals < max_evals && funcs[2]!=-1){
					actual.opt_fitness=EvaluateQAP(funcs[2], actual.opt_permu);
					cont = local_search(funcs, 2, &evals, max_evals, iteration, &actual, optimo, tabu, tabu_size);
					iteration++;
				}
				//Escapamos del óptimo local con la tercera función auxiliar.
				if(!cont && evals < max_evals && funcs[3]!=-1){
					actual.opt_fitness=EvaluateQAP(funcs[3], actual.opt_permu);
					cont = local_search(funcs, 3, &evals, max_evals, iteration, &actual, optimo, tabu, tabu_size);
					iteration++;
				}
		}
}
