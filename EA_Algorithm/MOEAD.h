#ifndef __COM_H_
#define __COM_H_


typedef struct strct_optimo{
	int *opt_permu;
	long double opt_fitness;
}strct_optimo;

void MOEAD(strct_optimo* optimo, int subproblems, int neighsize, long double coefficients[][3], double mutation_rate, int max_evals);

#endif
