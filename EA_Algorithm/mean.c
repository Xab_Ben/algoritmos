#include "globals.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

//Cálculo del fitness medio para las funciones objetivo de la descomposición.
long double evaluate_mean(int func){
    int n, i, j, p, q;
    long double fitness, distAB, mult, div;

    n = PermuSize;

    if(func==1){
      div=2*n;
      mult=-1;
    }else if(func==2){
      div=2*(n-2);
      mult=(long double)(n-3)/(long double)(n-1);
    }else{
      div=n*(n-2);
      mult=1;
    }

    fitness = 0.0;

	  for( i = 0; i < n;i++){
		    for( j = 0; j < n; j++){
             distAB = zero_dist_matrix[i][j];
             if(i==j) continue;
			       for( p = 0; p < n; p++){
				           for( q = 0; q < n; q++){
                        if(p==q) continue;
                        fitness = fitness + mult * distAB * zero_flow_matrix[p][q];
				           }
             }
		    }
	  }

    fitness = fitness/div;

    if(func==3){
      for (i=0;i<n;i++){
          distAB = dist_matrix[i][i];
          for( p = 0; p < n; p++){
              fitness = fitness + ((long double)1/(long double)n) * distAB * flow_matrix[p][p];
          }
      }
    }

    return (fitness);
}
