//Swap entre dos long doubles.
void swap1(long double *xp, long double *yp)
{
    long double temp = *xp;
    *xp = *yp;
    *yp = temp;
}

//Swap entre dos enteros.
void swap2(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

//Se ordenan dos arrays en función de uno de ellos. Long double - entero.
void bubbleSortDouble(long double arr1[], int arr2[], int n)
{
    int i, j;
    for (i = 0; i < n-1; i++){
      for (j = 0; j < n-i-1; j++){
          if (arr1[j] > arr1[j+1]){
              swap1(&arr1[j], &arr1[j+1]);
              swap2(&arr2[j], &arr2[j+1]);
          }
      }
    }
}

//Se ordenan dos arrays en función de uno de ellos. Entero - entero.
void bubbleSortInt(int arr1[], int arr2[], int n)
{
    int i, j;
    for (i = 0; i < n-1; i++){
      for (j = 0; j < n-i-1; j++){
          if (arr1[j] > arr1[j+1]){
              swap2(&arr1[j], &arr1[j+1]);
              swap2(&arr2[j], &arr2[j+1]);
          }
      }
    }
}
