#ifndef LocalSearch_AllProblems_globals_h
#define LocalSearch_AllProblems_globals_h

extern char * Instance;
extern char * Weights;
extern int PermuSize, Repetition, N_Sub, T_Nei, MaxEvals;
extern double Mutation_Rate;
extern long double ** flow_matrix;
extern long double ** zero_flow_matrix;
extern long double ** dist_matrix;
extern long double ** zero_dist_matrix;

#endif
