#include "MOEAD.h"
#include "RandomPerm.h"
#include "EvaluateQAP.h"
#include "globals.h"
#include "sort.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

char * Instance;
char * Weights;
int PermuSize, Repetition, N_Sub, T_Nei, MaxEvals;
double Mutation_Rate;
long double ** flow_matrix;
long double ** zero_flow_matrix;
long double ** dist_matrix;
long double ** zero_dist_matrix;

//Lee la instancia.
void readMatrix(long double **mat,  FILE *fd){
    int i,j,a;

    for(i=0;i<PermuSize;i++){
        for(j=0;j<PermuSize;j++){
            fscanf(fd,"%d\t",&a);
            mat[i][j] = (long double) a;
        }
    }
}

//Evalua una repetición del algoritmo.
void evalRandomSample(){

    int i;
    FILE *fw;
    long double coefficients[N_Sub][3];
    long double x, y, z;
    int random_row[N_Sub];
    int subproblems[N_Sub];
    int random, max_row, index;
	  strct_optimo optimo;

	  optimo.opt_permu=malloc(PermuSize*sizeof(int));

    fw=fopen(Weights,"r");

    if(fw==NULL){
      printf("El fichero especificado no existe.\n");
      exit(4);
    }

    fscanf(fw,"%d\t%d\n",&random,&max_row);

    //Se lee el fichero de pesos.
    if(random){
        for(i=0;i<N_Sub;i++){
            random_row[i] = rand()%max_row;
            subproblems[i] = i;
        }
        bubbleSortInt(random_row,subproblems,N_Sub);

        index = 0;

        for(i=0;i<max_row;i++){
            fscanf(fw,"%Lf,%Lf,%Lf\n",&x,&y,&z);
            while(index<N_Sub && random_row[index]==i){
                coefficients[subproblems[index]][0]=x;
                coefficients[subproblems[index]][1]=y;
                coefficients[subproblems[index]][2]=z;
                index++;
            }
        }
    }else{
        for(i=0;i<N_Sub;i++){
            fscanf(fw,"%Lf,%Lf,%Lf\n",&coefficients[i][0],&coefficients[i][1],&coefficients[i][2]);
        }
    }

    fclose(fw);

    MOEAD(&optimo,N_Sub,T_Nei,coefficients,Mutation_Rate,MaxEvals);

    //Los resultados se imprimen con el siguiente formato: Instancia, Tipo de pesos, Repetición, Solución, Fitness, Tiempo.
    printf("%s , %s , %d , ", Instance, Weights, Repetition);
		for (int i=0; i<PermuSize; i++) {
			  printf("%d ", optimo.opt_permu[i]);
		}
    printf(", %Lf", optimo.opt_fitness);
}

//Función principal.
int main (int argc, char *argv[]) {

	  int i,j;
    FILE *fd;

    /*
     1.- Fichero con la instancia.
     2.- Numero de intento.
     3.- Cantidad subproblemas.
     4.- Tamano vecindario.
     5.- Mutation rate.
     6.- Fichero con los pesos.
     7.- Maximo numero de evaluaciones.
     */
    if(argc != 8){
      printf("Formato incorrecto.\n");
      exit(1);
    }

    Instance = argv[1];
    Repetition = atoi(argv[2]);
    N_Sub = atoi(argv[3]);
    T_Nei = atoi(argv[4]);
    Mutation_Rate = atof(argv[5]);
    Weights = argv[6];
    MaxEvals = atoi(argv[7]);

    if(N_Sub<=T_Nei){
      printf("Formato incorrecto. Subproblemas debe ser mayor que tamano de vecindario.\n");
      exit(2);
    }

    srand(Repetition);

    fd=fopen(Instance,"r");

    if(fd==NULL){
      printf("El fichero especificado no existe.\n");
      exit(3);
    }

    fscanf(fd,"%d",&PermuSize);

    dist_matrix = malloc(PermuSize*sizeof(long double*));
    zero_dist_matrix = malloc(PermuSize*sizeof(long double*));
    flow_matrix = malloc(PermuSize*sizeof(long double*));
    zero_flow_matrix = malloc(PermuSize*sizeof(long double*));
    for ( i=0; i<PermuSize; i++) {
		    dist_matrix[i] = (long double *)malloc(PermuSize*sizeof(long double));
        zero_dist_matrix[i] = (long double *)malloc(PermuSize*sizeof(long double));
        flow_matrix[i] = (long double *)malloc(PermuSize*sizeof(long double));
        zero_flow_matrix[i] = (long double *)malloc(PermuSize*sizeof(long double));
	  }

    readMatrix(dist_matrix, fd);
    readMatrix(flow_matrix, fd);

    fclose(fd);

    for(i=0; i<PermuSize; i++){
      for(j=0; j<PermuSize; j++){
        if(i!=j){
          zero_dist_matrix[i][j] = dist_matrix[i][j];
          zero_flow_matrix[i][j] = flow_matrix[i][j];
        }else{
          zero_dist_matrix[i][j] = 0.0;
          zero_flow_matrix[i][j] = 0.0;
        }
      }
    }

    struct timeval start, stop;
    double secs = 0;
    gettimeofday(&start, NULL);

    evalRandomSample();

    gettimeofday(&stop, NULL);
    secs = (double)(stop.tv_usec - start.tv_usec) / 1000000 + (double)(stop.tv_sec - start.tv_sec);
    printf(" , %f\n", secs);
}
