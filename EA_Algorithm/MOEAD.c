#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "MOEAD.h"
#include "EvaluateQAP.h"
#include "RandomPerm.h"
#include "globals.h"
#include "sort.h"
#include "mean.h"

//Actualiza mejor solucion.
void check_best(int *solution, strct_optimo *optimo){
  int i;
  long double f0;
  f0 = EvaluateQAP(0,solution);
  if(optimo->opt_permu[0]==-1||f0<optimo->opt_fitness){
      for(i=0;i<PermuSize;i++){
          optimo->opt_permu[i] = solution[i];
      }
      optimo->opt_fitness=f0;
  }
}

//Crossover entre dos soluciones padres.
int * crossover(int * parent1, int * parent2){
    int i, j, point, aux, flag;
    int *child;
    child = malloc(PermuSize*sizeof(int));
    point = rand()%(PermuSize-1);
    for(i=0;i<=point;i++)
      child[i] = parent1[i];
    for(i=point+1;i<PermuSize;i++){
        aux = parent2[i];
        flag=1;
        while(flag){
            flag=0;
            for(j=0;j<=point;j++){
              if(aux==child[j]){
                  aux=parent2[j];
                  flag=1;
                  break;
              }
            }
        }
        child[i]=aux;
    }
    return child;
}

//Reproduccion. Tomamos dos soluciones de subproblemas vecinos.
int * reproduction(int *solutions[], int neighbors[], int neighsize){
    int i, j;
    i = rand()%neighsize;
    j = rand()%neighsize;
    while(i==j) j = rand()%neighsize;
    return crossover(solutions[neighbors[i]],solutions[neighbors[j]]);
}

//Mutacion swap.
void mutate(int * solution){
    int i, j, aux;
    i = rand()%PermuSize;
    j = rand()%PermuSize;
    while(i==j) j = rand()%PermuSize;
    aux = solution[i];
    solution[i] = solution[j];
    solution[j] = aux;
}

//Valor absoluto.
long double absolute(long double i){
  if(i<0) return -i;
  else return i;
}

//Calculo del Tchebycheff Approach.
long double calculate(long double f_solution[], long double lambda[], long double z[]){
    long double max, aux;
    max = lambda[0]*absolute(f_solution[0]-z[0]);
    aux = lambda[1]*absolute(f_solution[1]-z[1]);
    if(aux>max) max = aux;
    aux = lambda[2]*absolute(f_solution[2]-z[2]);
    if(aux>max) max = aux;
    return max;
}

//Multi-Objective Algorithm based on Decomposition.
void MOEAD(strct_optimo* optimo, int subproblems, int neighsize, long double coefficients[][3], double mutation_rate, int max_evals){
    int neighbors[subproblems][neighsize];
    int neighbor;
    int *solutions[subproblems];
    int *y;
    long double f_solutions[subproblems][3];
    long double f_y[3];
    long double z[3];
    int i,j,k,q,iteration,evals;
    long double distances[subproblems];
    int possible_neighbors[subproblems];

    //Calculamos distancia euclidea entre vectores de coeficientes, los ordenamos de menor a mayor en cada caso y establecemos el vecindario de cada subproblema.
    for(i=0;i<subproblems;i++){
        for(j=0;j<subproblems;j++){
            distances[j]= sqrtl(powl(coefficients[i][0]-coefficients[j][0],2)+powl(coefficients[i][1]-coefficients[j][1],2)+powl(coefficients[i][2]-coefficients[j][2],2));
            possible_neighbors[j]= j;
        }
        bubbleSortDouble(distances,possible_neighbors,subproblems);
        for(j=0;j<neighsize;j++){
            neighbors[i][j] = possible_neighbors[j];
        }
    }
    //Calculamos el punto de referencia z. En este caso, se trata del valor de fitness medio para cada uno de los landscapes.
    for(i=0;i<3;i++)
        z[i]=evaluate_mean(i+1);
    //Marcamos el optimo como vacio.
    optimo->opt_permu[0] = -1;
    //Inicializamos poblacion con soluciones aleatorias.
    evals=0;
    for(i=0;i<subproblems;i++){
        if(evals>=max_evals) break;
        solutions[i]= malloc(PermuSize*sizeof(int));
        RandomPerm(solutions[i]);
        for(j=0;j<3;j++){
            f_solutions[i][j]=EvaluateQAP(j+1,solutions[i]);
            if(f_solutions[i][j]<z[j]) z[j]=f_solutions[i][j];
        }
        evals+=1;
        //Actualizamos mejor solucion.
        check_best(solutions[i],optimo);
    }
    iteration=0;
    //Bucle principal.
    while(evals<max_evals){
      //printf("%d , %Lf\n",iteration,optimo->opt_fitness);
      iteration++;
      for(j=0;j<subproblems;j++){
          if(evals>=max_evals) break;
          //Reproduccion aleatoria tomando dos vecinos del subproblema.
          y=reproduction(solutions, neighbors[j], neighsize);
          //Mutamos con cierta probabilidad.
          if(mutation_rate > (double)rand()/(double)RAND_MAX) mutate(y);
          for(k=0;k<3;k++){
            f_y[k]=EvaluateQAP(k+1,y);
            if(f_y[k]<z[k]) z[k]=f_y[k];
          }
          evals+=1;
          //Actualizamos vecindario.
          for(k=0;k<neighsize;k++){
              neighbor=neighbors[j][k];
              //Calculamos utilizando el Tchebycheff Approach si la nueva solucion es mejor que la actual para cada subproblema.
              if(calculate(f_y,coefficients[neighbor],z)<calculate(f_solutions[neighbor],coefficients[neighbor],z)){
                  for(q=0;q<PermuSize;q++)
                      solutions[neighbor][q]=y[q];
                  for(q=0;q<3;q++)
                      f_solutions[neighbor][q]=f_y[q];
              }
          }
          //Actualizamos mejor solucion.
          check_best(y,optimo);
          free(y);
      }
    }
}
